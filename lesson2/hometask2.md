Написать скрипт, который создает не пустые файлы на одной
машине и передает их на другую машину. Скрипт запускается по
CRON на первой машине. Также скрипт управляет временем
хранения файлов на второй машине, которое не должно
превышать одной недели.
