Создаем директорию с проектом #mkdir project
Инициируем Вагрант #vagrant init
Редактируем Vagrantfile для создания 2-х ВМ
Запускаем ВМ #vagrant up
Входим на первую ВМ #vagrant ssh node1
Создаем пару ключей #ssh-keygen
Переходим на 2-ю ВМ 
Настраиваем sshd #cd /etc/ssh/sshd_config
	Раскоментируем строку PermitRootLogin "no" и изменяем на "yes"
	Разрешаем аутентификацию по поролю PasswordAuthentication изменяем "no" на "yes"
	Перезпускаем #service sshd restart
	Создаем пороль для рута #passwd root
	Вводим придуманный пороль
Передаем ключи на вторую машину #ssh-copy-id root@192.168.1.26
Пишем скрипт 
Делаем скрипт исполняемым используя #sudo chmod +x script.sh
Запускаем скрипт на исполнение с помощью cron  #crontab -e
Устанавливаем выполнение скрипта раз в сутки(в 10 часов утра) # 0 10 * * * ~/script.sh
Сохраняем 
