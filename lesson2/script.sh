#!/bin/bash
vremia=$(date +%H:%M) #создаем переменную 
base64 /dev/urandom | head -c 10000001 > noempty.$vremia.txt #создаем не пустой файл
 rsync -avz /root/*.txt root@192.168.1.26:/home/vagrant/NoemptyFiles # копируем файлы на удаленную машину в директорию NoemptyFiles 
find /root/ -name *.txt -type f -mtime +7 -delete #ищем файлы старше 7 дней и удаляем на локальной машине
rsync -avz /root/ --delete root@192.168.1.26:/home/vagrant/NoemptyFiles #синхронизируем локальную директорию root с удаленной NoemptyFiles, путем удаления файлов , которых нет в оригинальной директории 

