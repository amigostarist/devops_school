Создаем директорию с проектом #mkdir project
Инициируем Вагрант #vagrant init
Редактируем Vagrantfile для создания ВМ
Запускаем ВМ #vagrant up
Входим в ВМ #vagrant ssh nodedb1
Обновляем репозитории # sudo apt update
Устанавливаем пакет PostgreSQL #sudo apt install postgresql postgresql-contrib -y
Переключаемся под пользователя Postgres #sudo su postgres 
Вызываем командный интерпретатор #psql
Проверим наличие баз данных #\l
Проверим наличие пользователей #\du
Создадим нового пользователя #CREATE USER user1 WITH PASSWORD '123';
Создадим новую базу данных #CREATE DATABASE test1;
Выходим из командного интерпретатора postgres #\q
Выходим из под пользователя postgres #exit or (CTRL+D)
Выбираем место для временного храннения репозитория #cd /tmp
Клонируем из github не пустую базу данных #git clone https://github.com/pthom/northwind_psql.git
Переходим в директорию с репозиторием #cd northwind_psql/
Входим под пользователем postgres #sudo su postgres
Указываем в какую базу какой дамп будем заливать #psql -d test1 -f northwind.sql
Переходим в интерпретатор #psql
Проверяем какие базы есть #\l
Подключаемся к базе #\c test1
Просматриваем какие таблицы есть в базе #\dt
Выходим из интерпретатора #\q
Делаем дамп базы #pg_dump test1 > /tmp/db1.sql
#Сайт с документацией по postgrsql на русском языке http://postgrespro.ru

#подготовка стрестеста с помощью утилиты pgbench#

Открываем файл #/etc/postgressql/12/main/pg_hba.conf (файл в котором задаются разрешения: пользователей, прилодений и др.)
Меняем методы локального доступа на trust
Сохраняем 
Перезагружаем службу #sudo service postgresql restart
Переходим за пользователя postgres  #su postgres
Подготовка # pgbench -h(хост) localhost -U(пользователь) postgres -i -s(добавление строк с которыми будет работать pgbench) 100 test1 
pgbench -h localhost -U postgres -i -s 100 test1

Открываем еще одну вкладку терминала
Входим на nodedb1 #vagrant ssh nodedb1
Обновляем репозитории # sudo apt update -y
Устанавливаем утилиту мониторинга баз данных # sudo apt install pg-activity -y 
Переходим под пользователя postgres # sudo su postgres
запускаем утилиту # pg_activity

переходим на первую вкладку и запускаем стрестест #pgbench -h localhosn -p 5432 -U postgres -c(количество клиентов) 50 -j(кол-во потоков) 2 -P(показыввать прогресс каждые сек) 60 -T(время теста в сек) 600 test1 
Во второй вкладке наблюдаем за нагрузкой через pg_activity

# Настройка репликации #

Входим на вторую ВМ #vagrant ssh nodedb2
#sudo apt update
#sudo apt install postgresql -y
На мастер ноде nodedb1 переходим в /etc/postgresql/12/main
Редактируем файл postgresql.conf #sudo vim postgresql.conf
    Находим параметр listen_addresses = 'localhost' меняем '*'
    Раскоментируем wal_level = replica
    Раскомментирем wal_log_hints = off меняем на on 
    Раскомментирем max_wal_senders = 10 (максимальное кол-во реплик)
    Раскомментирем wal_keep_segments = 64 (максимальное кол-во журналов)
    Раскомментирем hot_standby = on (этот параметр означает можно ли будет подлючаться к серверу master-у и выполнять запросы в процесссе востановления)
    Сохраняем
Редактируем файл postgresql.conf #sudo vim pg_hba.conf
    # Allow replication connections from localhost, by a user with the
    # replication privilege.

    host    all             all             192.168.100.26/32       md5
    host    replication     postgres        192.168.100.26/32       md5
    Сохраняем
    перезапусакем postgresql #sudo service postgresql restart
    Заходим за пользователя postgres # sudo su postgres
    Входим в интерпретатор #psql
    Устанавливаем порольдля пользователя базы #ALTER ROLE postgres PASSWORD '123456';
Настраиваем слейв ноду nodedb2
    Останавливаем сервис # sudo service postgresql stop
    Переходим под пользователя postgres # sudo su postgres
    Удаляем файлы базы #rm -rf /var/lib/postgresql/12/main/*
    Создаем бекап #pg_basebackup -R -P -X stream -c fast -h 192.168.100.25 -U postgres -D ./main/
    Заходим под рут
    Запускаем сервис #service postgresql start
