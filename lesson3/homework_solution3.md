Создаем директорию с проектом #mkdir project
Инициируем Вагрант #vagrant init
Редактируем Vagrantfile для создания ВМ
Запускаем ВМ #vagrant up
Входим в ВМ #vagrant ssh maven
Обновляем репозитории # sudo apt update
Устанавливаем пакет Java #sudo apt install default-jdk -y
Устанавливаем пакет сборки maven # sudo apt install maven -y
Устанавливаем сервер war-приложений # sudo apt install tomcat9 -y
Проверяем работу Tomcat 
    -> Переходим в браузер 
    -> в строке поиска прописываем IP адрес нашей ВМ с указанием порта 8080 "192.168.100.55:8080"
Клонируем репозиторий # git clone https://github.com/boxfuse/boxfuse-sample-java-war-hello.git
Переходим в директорию с клонированым проектом # cd ~/boxfuse-sample-java-war-hello
Запускаем сборку #mvn package
Копируем получившийся артефакт в рабочуую директорию Tomcat # sudo cp ./target/hello-1.0.war /var/lib/tomcat9/webapps/
Проверям работу наше го приложения: 
    -> Переходим в браузер
    -> В строке поиска вводим адрес нашего сервера : порт / название артефакта # 192.168.100.55:8080/hello-1.0

Доп репозиторий для сборки https://github.com/daticahealth/java-tomcat-maven-example.git